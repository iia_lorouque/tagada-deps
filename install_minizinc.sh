#!/bin/bash

VERSION="2.7.2"

if [[ ! -d MiniZinc ]]; then
  mkdir MiniZinc
fi
curl -L https://github.com/MiniZinc/MiniZincIDE/releases/download/$VERSION/MiniZincIDE-$VERSION-bundle-linux-x86_64.tgz -o MiniZincIDE-$VERSION-bundle-linux-x86_64.tgz
tar -zxvf MiniZincIDE-$VERSION-bundle-linux-x86_64.tgz
mv MiniZincIDE-$VERSION-bundle-linux-x86_64 MiniZinc/$VERSION
rm MiniZincIDE-$VERSION-bundle-linux-x86_64.tgz
cd MiniZinc
  if [[ -f latest ]]; then
    rm latest
  fi
  ln -s $VERSION latest
cd ..
