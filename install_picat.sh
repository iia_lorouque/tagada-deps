#!/bin/bash

VERSION="34"

if [[ ! -d Picat ]]; then
  mkdir Picat
fi
mv Picat Picat.old
curl -L http://picat-lang.org/download/picat${VERSION}_linux64.tar.gz -o picat${VERSION}_linux64.tar.gz
tar -xvf picat${VERSION}_linux64.tar.gz
mv Picat Picat.old/$VERSION
rm picat${VERSION}_linux64.tar.gz
mv Picat.old Picat
cd Picat
  if [[ -f latest ]]; then
    rm latest
  fi
  ln -s $VERSION latest
  cd latest
    cd lib
      curl -L http://picat-lang.org/flatzinc/fzn_picat_sat.pi -o fzn_picat_sat.pi
      curl -L http://picat-lang.org/flatzinc/fzn_parser.pi -o fzn_parser.pi
      curl -L http://picat-lang.org/flatzinc/fzn_tokenizer.pi -o fzn_tokenizer.pi
      sed -i "s/    writeln(';'),/    println(';'),/" fzn_picat_sat.pi
    cd ..
  cd ..
cd ..
